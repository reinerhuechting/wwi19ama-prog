package main

import "fmt"

type Board [3][3]byte

func NewBoard() Board {
    var b Board
    
    // Das Feld mit Leerzeichen füllen
    for row:=0; row<3; row++ {
        for col:=0; col<3; col++ {
            b[row][col] = ' '
        }
    }
    
    return b
}

func (b Board) String() string {
    row_template := " %c | %c | %c \n"
    board_template := "%s---+---+---\n%s---+---+---\n%s"
    row1 := fmt.Sprintf(row_template, b[0][0], b[0][1], b[0][2])
    row2 := fmt.Sprintf(row_template, b[1][0], b[1][1], b[1][2])
    row3 := fmt.Sprintf(row_template, b[2][0], b[2][1], b[2][2])
    return fmt.Sprintf(board_template, row1, row2, row3)
}

func (b Board) checkRow(row int, c byte) bool {
    for i:=0; i<3; i++ {
        if b[row][i] != c {
            return false
        }
    }
    return true
}

func (b Board) checkAllRows(c byte) bool {
    for i:=0; i<3; i++ {
        if b.checkRow(i,c) {
            return true
        }
    }
    return false
}

func (b Board) checkColumn(col int, c byte) bool {
    for i:=0; i<3; i++ {
        if b[i][col] != c {
            return false
        }
    }
    return true
}

func (b Board) checkAllColumns(c byte) bool {
    for i:=0; i<3; i++ {
        if b.checkColumn(i,c) {
            return true
        }
    }
    return false
}

func (b Board) checkDiags(c byte) bool {
    if b[0][0] == c && b[1][1] == c && b[2][2] == c {
        return true
    } 
    if b[0][2] == c && b[1][1] == c && b[2][0] == c {
        return true
    }
    return false
}

func (b Board)checkWinner(c byte) bool {
    if b.checkAllRows(c) || b.checkAllColumns(c) || b.checkDiags(c) {
        return true
    }
    return false
}
