/** Definition des Spielfeld-Datentyps für TicTacToe.
 *
 *  Diese Datei enthält die Definition des Spielfeld-Datentyps sowie
 *  einige allgemeine Hilfsfunktionen zum Umgang mit dem Spielfeld.
 */

package main

import "fmt"

/// Definiere den Datentyp für das Spielfeld
type Board [3][3]byte

/// Initialisert das Spielfeld mit Leerzeichen.
func (b * Board) init() {
    for row:=0; row<3; row++ {
        for col:=0; col<3; col++ {
            b[row][col] = ' '
        }
    }
}

/// Liefert eine String-Repräsentation des Spielfelds.
func (b Board) String() string {
    row_template := " %c | %c | %c \n"
    board_template := "%s---+---+---\n%s---+---+---\n%s"
    row1 := fmt.Sprintf(row_template, b[0][0], b[0][1], b[0][2])
    row2 := fmt.Sprintf(row_template, b[1][0], b[1][1], b[1][2])
    row3 := fmt.Sprintf(row_template, b[2][0], b[2][1], b[2][2])
    return fmt.Sprintf(board_template, row1, row2, row3)
}

/// Gibt eine Variante des Bretts aus, die mit den Zahlen 1-9 gefüllt ist
func print_board_template() {
    var b Board

    var i byte = '1'
    for row:=0; row<3; row++ {
        for col:=0; col<3; col++ {
            b[row][col] = i
            i++
        }
    }
    
    fmt.Print(b)
}